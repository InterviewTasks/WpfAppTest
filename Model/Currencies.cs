﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTest.Model
{
    class Currencies
    {
        public String Name { get; set; }
        public Int32 Volume { get; set; }
        public Decimal Amount { get; set; }

        public Currencies() { }

        public Currencies(String name, Int32 volume, Decimal amount)
        {
            Name = name;
            Volume = volume;
            Amount = amount;
        }
    }
}
