﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace WpfAppTest
{
    public class LoadData : ILoadData
    {
        /// <summary>
        /// Получение данных от веб сервиса
        /// </summary>
        public LoadData() { }
        List<Model.Currencies> ILoadData.Do()
        {
            List<Model.Currencies> result = new List<Model.Currencies>();
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("http://phisix-api3.appspot.com/stocks.json");
            // {"name":"HOUSE PREF A","price":{"currency":"PHP","amount":101.30},"percent_change":-0.20,"volume":4440,"symbol":"8990P"}
            // В качестве параметра конструктора, поток содержащий данные json.

            // для проверка исключений
            // int s1 = 0; int dd = 4 / s1;

            using (var reader = new StreamReader(stream))
            {
                String json = reader.ReadToEnd();
                JsonFile.JsonFileRoot data = JsonConvert.DeserializeObject<JsonFile.JsonFileRoot>(json);
                foreach (var jsonElement in data.stock)
                {
                    Model.Currencies currencies = new Model.Currencies(jsonElement.name, jsonElement.volume, jsonElement.price.amount);
                    result.Add(currencies);
                }
            }
            return result;
        }
    }
}