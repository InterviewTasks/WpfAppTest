# This is a interview task.
The job was done using C#.NET, WPF, NLog, Newtonsoft.Json, Microsoft Visual Studio 2017, .NET Framework 4.6.1

Requirements:
1. Create a launch screen (show any icon 64x64).
2. Create the main screen, which is a table (currency list). The data must be retrieved from http://phisix-api3.appspot.com/stocks.json
3. The currency row consists of:
	- Name (string);
	- Price (integer);
	- Quantity (Number, 2 decimal).
	The height of the cell is 50.
4. The data in the table must be updated every 15 seconds.
5. Create a refresh button in the navigation menu for manual refresh.
