﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using NLog;


namespace WpfAppTest
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public App()
        {
            try
            {
                logger = LogManager.GetCurrentClassLogger();

                logger.Trace("Version: {0}", Environment.Version.ToString());
                logger.Trace("OS: {0}", Environment.OSVersion.ToString());
                logger.Trace("Command: {0}", Environment.CommandLine.ToString());
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка работы с логом!" +Environment.NewLine + e.Message);
                Current.Shutdown();
            }
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;
            logger.Info("");
            logger.Info("App start");
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = ((Exception)e.ExceptionObject);
            logger.Error("UnhandledException caught : " + ex.Message);
            logger.Error("UnhandledException StackTrace : " + ex.StackTrace);
            logger.Fatal("Runtime terminating: {0}", e.IsTerminating);
        }
    }
}
