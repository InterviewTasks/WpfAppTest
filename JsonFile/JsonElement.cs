﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTest.JsonFile
{
    class JsonElement
    {
        public JsonElement() { }

        public String name { get; set; }
        public JsonFilePrice price { get; set; }
        public Decimal percent_change { get; set; }
        public Int32 volume { get; set; }
        public String symbol { get; set; }
    }
}
