﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTest.JsonFile
{
    class JsonFilePrice
    {
        public JsonFilePrice() { }

        public String currency { get; set; }
        public Decimal amount { get; set; }
    }
}
