﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTest
{
    public interface IWindowWait
    {
        void FormShow(Double top, Double left, Double height, Double width);
        void FormClose();
    }
}
