﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace WpfAppTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        // Запрет завершения приложения
        public Boolean ExitEnabled = true;
        // таймер, запускающий обновлени источника данных
        System.Windows.Threading.DispatcherTimer dispatcherTimer;
        // Окно загрузки данных
        IWindowWait windowWait;
        // Фоновая задача
        private BackgroundWorker backgroundWorker;

        public WindowMain()
        {
            InitializeComponent();
            // из ресурсов XAML
            backgroundWorker = ((BackgroundWorker)FindResource("backgroundWorker"));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 15);
            GetDate();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!ExitEnabled) { e.Cancel = true; }
            App.logger.Info("App stop");
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            App.logger.Info("Manual data refresh");
            GetDate();
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            App.logger.Info("Auto Data refresh by timer");
            GetDate();
        }

        private void GetDate()
        {
            // установить запрет закрытия приложения
            ExitEnabled = false;
            // запрет запуска команд через UI
            ButtonRefresh.IsEnabled = false;
            ButtonExit.IsEnabled = false;
            // Окно запуска должно быть MostTop и по центру
            windowWait = new WindowWait();
            windowWait.FormShow(Top,Left,Height,Width);
            App.logger.Info("Get data: start");
            // запуск задачи в фоне
            backgroundWorker.RunWorkerAsync();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Информация о возникших исключениях уйдет в backgroundWorker_RunWorkerCompleted args(RunWorkerCompletedEventArgs e).Error
            // получили данные
            ILoadData loadData = new LoadData();
            // данные в результат для передачи в основной поток
            e.Result = loadData.Do();
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // Ошибка была сгенерирована обработчиком события DoWork
                MessageBox.Show(e.Error.Message, "Произошла ошибка");
                App.logger.Error("StackTrace caught : " + e.Error.Message);
                App.logger.Error("StackTrace : " + e.Error.StackTrace);
            }
            else
            {
                // назнаем массив источником данных
                dataGrid.ItemsSource = (List<Model.Currencies>)e.Result;
            }
            // отменить запрет закрытия приложения
            ExitEnabled = true;
            // разрешить запуск команд через UI
            ButtonRefresh.IsEnabled = true;
            ButtonExit.IsEnabled = true;
            // Окно запуска закрыть
            windowWait.FormClose();
            // Таймер обновления данных запустить
            dispatcherTimer.Start();
            App.logger.Info("Get data: completed");
        }
    }
}
