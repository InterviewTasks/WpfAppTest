﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppTest
{
    /// <summary>
    /// Логика взаимодействия для WindowLaunch.xaml
    /// </summary>
    public partial class WindowWait : Window, IWindowWait
    {
        public WindowWait()
        {
            InitializeComponent();
        }
        void IWindowWait.FormShow(Double top, Double left, Double height, Double width)
        {
            Top = top + (height - Height) / 2;
            Left = left + (width - Width) / 2;
            Show();
        }

        void IWindowWait.FormClose()
        {
            Close();
        }
    }
}
